import 'package:flutter/rendering.dart';

final List<Section> sections = [
  Section(
    id: 1,
    header: 'If you know me personally',
    text:
        '''Follow/add/message me on as many of the services below as you want and I'll follow/add/message you back.''',
    profiles: [
      ProfileInfo(
        serviceName: 'Mastodon',
        serviceLogo: AssetImage('assets/images/mastodon.png'),
        username: '@lig@fosstodon.org',
        url: 'https://fosstodon.org/@lig',
      ),
      ProfileInfo(
        serviceName: 'Matrix',
        serviceLogo: AssetImage('assets/images/matrix.png'),
        username: '@lig:matrix.org',
        url: 'https://matrix.to/#/@lig:matrix.org',
      ),
    ],
  ),
];

class Section {
  int id;
  String header;
  String text;
  List<ProfileInfo> profiles;

  Section({
    required this.id,
    required this.header,
    required this.text,
    required this.profiles,
  });
}

class ProfileInfo {
  String serviceName;
  AssetImage serviceLogo;
  String username;
  String url;

  ProfileInfo({
    required this.serviceName,
    required this.serviceLogo,
    required this.username,
    required this.url,
  });
}
