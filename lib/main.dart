import 'dart:html' show window;
import 'dart:ui' hide window;

import 'package:flutter/material.dart';

import 'package:xyz/content.dart';

void main() {
  runApp(XYZApp());
}

class XYZApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Serge Matveenko aka `lig`',
      theme: ThemeData.dark(),
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          constraints: BoxConstraints.loose(Size.fromWidth(800)),
          child: Scrollbar(
            child: SingleChildScrollView(
              child: ExpansionPanelList.radio(
                children: sections.map(buildSection).toList(),
              ),
            ),
          ),
        ),
      ),
    );
  }

  ExpansionPanelRadio buildSection(Section section) => ExpansionPanelRadio(
        value: section.id,
        canTapOnHeader: true,
        headerBuilder: (BuildContext context, bool isExpanded) {
          return ListTile(
            title: Text(section.header),
          );
        },
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(section.text),
              ),
              Wrap(
                children: section.profiles.map(buildProfile).toList(),
              ),
            ],
          ),
        ),
      );

  Container buildProfile(ProfileInfo profile) => Container(
        child: Card(
          elevation: 2,
          child: InkWell(
            borderRadius: BorderRadius.circular(4.0),
            onTap: () => window.open(profile.url, 'tab'),
            child: SizedBox(
              width: 200,
              height: 56,
              child: Stack(
                children: [
                  Positioned(
                    left: 56,
                    top: 12,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(profile.serviceName),
                        RichText(
                          text: TextSpan(
                            text: profile.username,
                            style:
                                Theme.of(context).textTheme.caption?.copyWith(
                              fontFeatures: [
                                FontFeature.tabularFigures(),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    left: 8,
                    top: 8,
                    child: Image(
                      image: profile.serviceLogo,
                      width: 40,
                      height: 40,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}
